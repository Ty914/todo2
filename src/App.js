import React from 'react';
import './App.css';

class App extends React.Component {
  constructor(props) {
    super(props)
    
    var dataSet = [
      {
        question: "Who is the NBA's all-time leading scorer?",
        answers: [
          "Michael Jordan",
          "Kareem Abdul-Jabbar",
          "Wilt Chamberlain",
          "Lebron James"
        ],
        correct: 1
      },
      {
        question: "Who is the NBA all-time leader in assists?",
            answers: [
              "Magic Johnson",
              "Lebron James",
              "Oscar Robertson",
              "John Stockton"
            ],
            correct: 3
      },
       {
            question: "Who is the NBA's all-time leader in 3's made?",
            answers: [
              "Reggie Miller",
              "Steph Curry",
              "Larry Bird",
              "Ray Allen"
            ],
            correct: 3
          },
          {
            question: "Who has played the most minutes in NBA Playoff History?",
            answers: [
              "Tim Duncan",
              "Kobe Bryant",
              "Lebron James",
              "Scottie Pippen"
            ],
            correct: 2
          },
          {
            question: "Which team has won the most NBA Championships (17)?",
            answers: [
              "Boston Celtics",
              "Chicago Bulls",
              "Los Angeles Lakers",
              "Golden State Warriors"
            ],
            correct: 0
          },
          {
            question: "What player has scored the most career points in the NBA Finals?",
            answers: [
              "Michael Jordan",
              "Lebron James",
              "Kareem Abdul-Jabbar",
              "Jerry West"
            ],
            correct: 3
          },
          {
            question: "Which of these players has NOT led the NBA in blocks for a single season?",
            answers: [
              "Rudy Gobert",
              "Shaquille O'Neal",
              "Anthony Davis",
              "Myles Turner"
            ],
            correct: 1
          },
          {
            question: "Who was the youngest player to win a scoring title (21)?",
            answers: [
              "Kobe Bryant",
              "Allen Iverson",
              "Kevin Durant",
              "Michael Jordan"
            ],
            correct: 2
          },
          {
            question: "Who has the most career triple-doubles?",
            answers: [
              "Oscar Robertson",
              "James Harden",
              "Magic Johnson",
              "Russell Westbrook"
            ],
            correct: 0
          },
          {
            question: "Who was the last player to record a quadruple-double?",
            answers: [
              "Hakeem Olajuwon",
              "Draymond Green",
              "Lebron James",
              "David Robinson"
            ],
            correct: 3
          },
          {
            question: "How many did you get right?",
            answers: [
              10,
               9,
               8,
            ]
          },
    ];
    
    this.state = {current:0, dataSet:dataSet, correct:0, incorrect:0}
    this.handleClick = this.handleClick.bind(this)
    
  } // end constructor
  
  handleClick(choice) {
    if (choice === this.state.dataSet[this.state.current].correct) {
      this.setState({correct: this.state.correct + 1})
    } else {
      this.setState({incorrect: this.state.incorrect + 1})
    }
    
    if (this.state.current === 10) {
      this.setState({current: 0})
      this.setState({incorrect: 0})
      this.setState({correct: 0})
    } else {
         this.setState({current: this.state.current + 1}) 
    }
  }
  
  render() {
    return(
      <>
      <div id="top">
      <h1>Dom's NBA Quiz</h1>
      <h4>Vol.1 - Records</h4>
      </div>
      <div id="pictures">
        <div id="pic1">
          <img src="https://www.nbcsports.com/chicago/sites/csnchicago/files/styles/article_hero_image/public/2020/04/08/jordan_dunk_contest.jpg?itok=9TYv7oNt" alt="basketball"></img>
        </div>
        <div id="pic2">
          <img src="https://www.si.com/.image/t_share/MTcwMzExMzEwNTc0MTAxODM5/lebron-dunk.jpg"alt="basketball"></img>
        </div>
        <div id="pic3">
          <img src="https://images-na.ssl-images-amazon.com/images/I/91OLK24o0ML._AC_SX522_.jpg"alt="basketball"></img>
        </div>
      </div>
      <div>
      <div id ="score">
        <ScoreArea correct={this.state.correct} incorrect={this.state.incorrect} />
      </div>
        <QuizArea handleClick={this.handleClick} dataSet={this.state.dataSet[this.state.current]} />
        </div>
      </>

    )
  }
}

function Question(props) {
  var style = {
    color: "red",
    width: 700,
    marginLeft: 150
  }
  return (
    <h2 style={style}>{props.dataSet.question}</h2>
  )
}

function Answer(props) {
  var style = {
    width: "100%",
    height: 40,
    color: "blue",
    marginLeft: 360
  }
  return(
    <div>
      <button style={style} onClick={() => props.handleClick(props.choice)}>{props.answer}</button>
    </div>
  )
}

function AnswerList(props) {
  var answers = []
  for (let i = 0; i < props.dataSet.answers.length; i++) {
    answers.push(<Answer choice={i} handleClick={props.handleClick} answer={props.dataSet.answers[i]} />)
  }
  return(
    <div>
      {answers}
    </div>
  )
}

function QuizArea(props) {
  var style = {
    width: "25%",
    display: "block",
    textAlign: "center",
    boxSizing: "border-box",
    float: "left",
    padding: "0 2em"
  }
  return(
    <div style={style}>
      <Question dataSet={props.dataSet} />
      <AnswerList dataSet={props.dataSet} handleClick={props.handleClick} />
    </div>
  )
}

function TotalCorrect(props) {
    var style = {
    display: "inline-block",
    padding: "1em",
    background: "#eee",
    margin: "0 1em 0 0"
  }
  return(
    <h2 style={style}>Correct: {props.correct}</h2>
  )
}

function TotalIncorrect(props) {
  var style = {
    display: "inline-block",    
    padding: "1em",
    background: "#eee",
    margin: "0 0 0 1em"
  }
  return(
    <h2 style={style}>Incorrect: {props.incorrect}</h2>
  )
}

function ScoreArea(props) {
  var style = {
    width: "100%",
    display: "block",
    textAlign: "left",
    float: "left",
    padding: "1em"
  }
  return(
    <div id="QA" style={style} >
      <TotalCorrect correct={props.correct} />
      <TotalIncorrect incorrect={props.incorrect} />
    </div>
  )
}
export default App;